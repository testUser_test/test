package com.test;

import org.junit.Assert;
import org.junit.Test;



public class CalculatorTest {

    @Test
    public void add() {
        Calculator calculator = new Calculator();
        Assert.assertSame(4, calculator.add(2, 2));
        Assert.assertSame(6, calculator.add(2, 4));
        //Assert.assertSame(2147483649L, calculator.add(Integer.MAX_VALUE, 2));
    }
}